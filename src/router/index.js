import Vue from "vue";
import VueRouter from "vue-router";
// import HomeView from "../views/HomeView.vue";
import Home from "@/components/Home.vue";
import login from "@/components/login.vue";
import HomeView from "@/views/HomeView.vue";
import NProgress from 'nprogress'
import 'nprogress/nprogress.css'// nprogress样式文件
NProgress.configure({
  easing: 'ease',  // 动画方式
  speed: 300,  // 递增进度条的速度
  showSpinner: false, // 是否显示加载ico
  trickleSpeed: 200, // 自动递增间隔
  minimum: 0.3 // 初始化时的最小百分比
})

Vue.use(VueRouter);

const routes = [
  {
    //这里需要将根目录默认为Home，方便实现用户在保持登录 状态下再次登录时直接跳转至主页面
    path: "/",
    redirect: {
      name: "Home"
    }
  },
  {
    path: "/Home",
    name: "Home",
    component: Home,
    redirect: '/HomeView',
    meta: {
      title: '首页',
    },
    children: [

      {
        path: "/HomeView",
        name: "HomeView",
        component: HomeView,

      },
      {
        path: "/Homemap",
        name: "Homemap",
        component: () =>
          import("@/views/Homemap.vue"),
        meta: {
          title: '商品 / 商品分类',
        },
      },
      {
        path: "/Homemaptou",
        name: "Homemaptou",
        component: () =>
          import("@/views/Homemaptou.vue"),
        meta: {
          title: '商品 / 商品大全',
        },
      },
      {
        path: "/Homeabout",
        name: "Homeabout",
        component: () =>
          import("@/views/Homeabout.vue"),
        meta: {
          title: '品类 / 品类分类',

        },
      },
      {
        path: "/HomeMassage",
        name: "HomeMassage",
        component: () =>
          import("@/views/HomeMassage.vue"),
        meta: {
          title: '订单 / 订单分类',

        },
      },
      {
        path: "/Homeage",
        name: "Homeage",
        component: () =>
          import("@/views/Homeage.vue"),
        meta: {
          title: '用户 / 用户分类',

        },
      },
    ]
  },
  {
    path: "/login",
    name: "login",
    component: login,
    meta: {
      title: '登录',
    },
  }
  , {
    path: "/register",
    name: "register",
    component: () =>
      import("@/components/register.vue"),
    meta: {
      title: '注册',
    },
  },

];

const router = new VueRouter({
  mode: 'history',
  routes
});


router.beforeEach((to, from, next) => {
  //登录及注册页面可以直接进入,而主页面需要分情况
  if (to.path == '/login') {
    // 每次切换页面时，调用进度条
    NProgress.start();
    next();
    //当路由进入后：关闭进度条
    NProgress.done()
    console.log(localStorage.s);
  }
  else if (to.path == '/register') {
    NProgress.start();
    next();
    NProgress.done()
  }
  else {
    if (from.path == "/login")//从登录页面可以直接通过登录进入主页面
    {
      NProgress.start();
      next();
      NProgress.done()
    }
    else {
      //从/进入,如果登录状态是true，则直接next进入主页面
      if (localStorage.s === "true") {
        NProgress.start();
        next();
        NProgress.done()
        console.log(localStorage['s'])
      }
      else {//如果登录状态是false，那么跳转至登录页面,需要登录才能进入主页面
        NProgress.start();
        next('/login');
        NProgress.done()
        console.log("需要登录")
      }
    }
  }

})

export default router;
