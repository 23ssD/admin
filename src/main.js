import Vue from "vue";
// import { Button } from "element-ui";
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import "normalize.css";
import App from "./App.vue";
import router from "./router";
import store from "./store";

// import './rem/res';
Vue.use(ElementUI);
Vue.config.productionTip = false;
// Vue.use(Button);
new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount("#app");
